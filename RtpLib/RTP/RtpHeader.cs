﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RtpLib.RTP
{
    public class RtpHeader
    {
        public int Version = 2;
        public bool Padding = false;
        public bool Extension = false;
        public uint CsrcCount = 0;
        public bool Marker = false;
        public int PayloadType = 0;
        public ushort SequenceNumber = 0;
        public uint Timestamp = 125463;
        public uint Ssrc = 0;
        public int[] Csrc = null;

        public RtpHeader()
        {

        }

        public void CreateHeader(int version, bool padding, bool extension, uint csrcCount, bool marker, int payloadType,
            ushort sequenceNumber, uint timestamp, uint ssrc, int[] csrc)
        {
            Version = version;
            Padding = padding;
            Extension = extension;
            CsrcCount = csrcCount;
            Marker = marker;
            PayloadType = payloadType;
            SequenceNumber = sequenceNumber;
            Timestamp = timestamp;
            Ssrc = ssrc;
            Csrc = csrc;
        }

        public String GenerateHeader()
        {

            return null;
        }

        private void ParseFromByte(byte[] buffer)
        {
            Convert.ToString(buffer[0]);
        }

        public byte[] ToByte()
        {
            byte[] HeaderBuffer = new byte[12];
            int Counter = 0;

            String TmpCsrcCount = Convert.ToString(CsrcCount, 2);
            HeaderBuffer[Counter++] = (byte) Convert.ToInt32(Convert.ToString(Version, 2) + "" + Convert.ToUInt16(Padding) + "" + Convert.ToUInt16(Extension) +
                "" + new String('0', 4 - TmpCsrcCount.Length) + TmpCsrcCount, 2);

            String TmpPayloadType = Convert.ToString(PayloadType, 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32((Convert.ToUInt16(Marker) + new String('0', 7 - TmpPayloadType.Length) + TmpPayloadType), 2);
            
            String TmpSequenceNumber = Convert.ToString(SequenceNumber, 2).PadLeft(16, '0');
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpSequenceNumber.Substring(0, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpSequenceNumber.Substring(8, 8), 2);

            String TmpTimestamp = Convert.ToString(Timestamp, 2).PadLeft(32, '0');
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpTimestamp.Substring(0, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpTimestamp.Substring(8, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpTimestamp.Substring(16, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpTimestamp.Substring(24, 8), 2);

            String TmpSsrc = Convert.ToString(Ssrc, 2).PadLeft(32, '0');
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpSsrc.Substring(0, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpSsrc.Substring(8, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpSsrc.Substring(16, 8), 2);
            HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpSsrc.Substring(24, 8), 2);

            if (Csrc != null)
            {
                foreach(int CsrcItem in Csrc)
                {
                    String TmpCsrc = Convert.ToString(CsrcItem, 2).PadLeft(32, '0');
                    HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpCsrc.Substring(0, 8), 2);
                    HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpCsrc.Substring(8, 8), 2);
                    HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpCsrc.Substring(16, 8), 2);
                    HeaderBuffer[Counter++] = (byte)Convert.ToInt32(TmpCsrc.Substring(24, 8), 2);
                }
            }
            
            return HeaderBuffer;
        }


    }
}
