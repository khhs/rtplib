﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RtpLib.RTP
{
    class RtpSession
    {
        private RtpPacket OutgoingRtpPacket;
        private IPEndPoint SourceEndPoint;
        private IPEndPoint DestinationEndPoint;
        private Socket AudioSocket;
        private byte[] Buffer;
        private int Mtu = 4096;

        //Header
        private ushort SequenceNumber = 0;
        private uint TimeStamp;

        public RtpSession(IPEndPoint sourceEndPoint, IPEndPoint destinationEndPoint)
        {
            SourceEndPoint = sourceEndPoint;
            DestinationEndPoint = destinationEndPoint;
        }

        public void StartRtpSession()
        {
            AudioSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            AudioSocket.Bind(SourceEndPoint);
            AudioSocket.Connect(DestinationEndPoint);

            Buffer = new byte[Mtu];
            AudioSocket.BeginReceive(Buffer,0, Mtu, SocketFlags.None, new AsyncCallback(AudioSocket_PacketReceived), null);
        }

        private void AudioSocket_PacketReceived(IAsyncResult ar)
        {
            int BytesReceived = AudioSocket.EndReceive(ar);

            Console.WriteLine(BytesReceived);

            AudioSocket.BeginReceive(Buffer, 0, Mtu, SocketFlags.None, new AsyncCallback(AudioSocket_PacketReceived), null);
        }

        public void SendData(byte[] dataBuffer)
        {
            OutgoingRtpPacket = new RtpPacket();
            OutgoingRtpPacket.GenerateHeader(2, false, false, 0, false, 0, SequenceNumber,
                (uint)DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds, 0, null);

            //AudioSocket.BeginSend();
        }
    }
}
