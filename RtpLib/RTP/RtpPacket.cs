﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RtpLib.RTP
{
    public class RtpPacket
    {
        public RtpHeader Header;
        public byte[] DataBuffer;

        public RtpPacket()
        {
            Header = new RtpHeader();
            DataBuffer = new byte[172];
        }

        public byte[] ToByte()
        {
            byte[] HeaderBuffer = Header.ToByte();
            byte[] Buffer = Encoding.UTF8.GetBytes("Hei");

            Array.Copy(HeaderBuffer, 0, DataBuffer, 0, HeaderBuffer.Length);
            Array.Copy(Buffer, 0, DataBuffer, HeaderBuffer.Length, Buffer.Length);

            return DataBuffer;
        }

        public void GenerateHeader(int version, bool padding, bool extension, uint csrcCount, bool marker, int payloadType,
            ushort sequenceNumber, uint timestamp, uint ssrc, int[] csrc)
        {
            Header.CreateHeader(version, padding, extension, csrcCount, marker, payloadType, sequenceNumber, timestamp, ssrc, csrc);
        }
    }
}
