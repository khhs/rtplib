﻿using RtpLib.RTP;
using SkaugSIP.SIP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine((uint)DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);

            /*String s = Convert.ToString(2, 2);
            ushort s1 = Convert.ToUInt16(false);
            ushort s2 = Convert.ToUInt16(false);

            Console.WriteLine(s);
            Console.WriteLine(s1);
            Console.WriteLine(s2);
            Console.WriteLine(Convert.ToInt32("1000", 2)+"\n");
            String sa = Convert.ToString(0, 2);
            
            Console.WriteLine((byte)(2 << 6 | 0 << 5 | 0 & 0xF));
            Console.WriteLine(Convert.ToInt16(Convert.ToString(2, 2) + Convert.ToUInt16(false) + Convert.ToUInt16(false) + new String('0', 4 - sa.Length) + sa, 2));

            
            Console.WriteLine(Convert.ToByte(10));*/

            /*int sta = 65535;
            String TmpSequenceNumber = Convert.ToString(sta, 2).PadLeft(16, '0');
            Console.WriteLine(TmpSequenceNumber);

            var bc = (byte)(sta >> 8);
            var bca = (byte)(sta & 0xFF);
            Console.WriteLine(bc);
            Console.WriteLine(bca);

            Console.WriteLine(TmpSequenceNumber.Substring(0, 8));
            Console.WriteLine((byte)Convert.ToInt32(TmpSequenceNumber.Substring(0, 8), 2));
            Console.WriteLine(TmpSequenceNumber.Substring(8, 8));
            Console.WriteLine((byte)Convert.ToInt32(TmpSequenceNumber.Substring(8, 8), 2));*/

            /*Socket Sock = new Socket(SocketType.Dgram, ProtocolType.Udp);
            //Sock.Bind(new IPEndPoint(IPAddress.Parse("localhost"), 54524));
            Sock.Connect(IPAddress.Parse("158.39.188.198"), 5060);

            RtpPacket rh = new RtpPacket();
            byte[] bb = rh.ToByte();
            Console.WriteLine(Sock.Send(bb, bb.Length, SocketFlags.None));*/

            Register();
            Console.ReadLine();
        }

        static PhoneClient pc = new PhoneClient("demosip.ipco.no", "+47580012000498", "Vel2016Ferd!");
        private static void Register()
        {
            pc.OnRegistered += Pc_OnRegistered;
            pc.Register();
        }

        private static void Pc_OnRegistered(object sender, string e)
        {
            Console.WriteLine(e);
            if (e.ToLower().Equals("registered"))
            {
                pc.UnRegister();
            }
        }
    }
}
